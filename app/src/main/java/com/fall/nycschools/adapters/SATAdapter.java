package com.fall.nycschools.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fall.nycschools.R;
import com.fall.nycschools.models.SAT;

import java.util.List;

/**
 * This adapter will handle displaying SAT scores in its recycler view.
 */
public class SATAdapter extends RecyclerView.Adapter<SATAdapter.SchoolViewHolder>{

    private List<SAT> listSAT;
    private final Context context;

    public SATAdapter(Context context, List<SAT> listSAT) {
        this.context = context;
        this.listSAT = listSAT;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SchoolViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_sat, parent, false)
        );
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        SAT sat = listSAT.get(position);
        holder.textViewSchoolNAme.setText(sat.getSchool_name());
        holder.textViewScoreMath.setText("Score Math: " + sat.getSat_math_avg_score());
        holder.textViewScoreReading.setText("Score Reading: " + sat.getSat_critical_reading_avg_score());
        holder.textViewScoreWriting.setText("Score Writing: " + sat.getSat_writing_avg_score());
    }

    @Override
    public int getItemCount() {
        return listSAT.size();
    }

    public void removeItem(int position) {
        listSAT.remove(position);
        notifyItemRemoved(position);
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewSchoolNAme;
        private TextView textViewScoreMath;
        private TextView textViewScoreReading;
        private TextView textViewScoreWriting;

        SchoolViewHolder(View itemView) {
            super(itemView);

            textViewSchoolNAme = itemView.findViewById(R.id.tv_school_name);
            textViewScoreMath = itemView.findViewById(R.id.tv_score_math);
            textViewScoreReading = itemView.findViewById(R.id.tv_score_reading);
            textViewScoreWriting = itemView.findViewById(R.id.tv_score_writing);
        }
    }

}
