package com.fall.nycschools.services.clients;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class build a retrofit object alongside with GsonConverterFactory which allows
 * to fetch and get data back easily.
 */
public class SchoolClient {
    public static Retrofit getClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
