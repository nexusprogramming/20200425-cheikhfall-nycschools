package com.fall.nycschools.services.interfaces;

import com.fall.nycschools.models.SAT;
import com.fall.nycschools.models.School;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SchoolApi {

    //End point for getting school's data
    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchool();

    //End point for getting SAT's data
    @GET("f9bf-2cp4.json")
    Call<List<SAT>> getSAT();
}
