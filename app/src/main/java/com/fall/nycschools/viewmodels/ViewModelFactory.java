package com.fall.nycschools.viewmodels;

import android.app.Activity;
import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.fall.nycschools.ui.fragments.SATFragment;
import com.fall.nycschools.ui.fragments.SchoolFragment;

/**
 * This class is a wrapper which helps initializing the viewmodel.
 */
public class ViewModelFactory implements ViewModelProvider.Factory {

    private Context context;
    public ViewModelFactory(Activity activity, SchoolFragment schoolFragment){
        this.context = activity;
    }

    public ViewModelFactory(Activity activity, SATFragment SATFragment){
        this.context = activity;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new SchoolViewModel(context);
    }

}
