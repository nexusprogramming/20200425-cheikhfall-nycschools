package com.fall.nycschools.ui.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fall.nycschools.R;
import com.fall.nycschools.adapters.SchoolAdapter;
import com.fall.nycschools.models.School;
import com.fall.nycschools.viewmodels.SchoolViewModel;
import com.fall.nycschools.viewmodels.ViewModelFactory;

import java.util.List;

/**
 * This fragment display all schools fetched from the API by observing the viewmodel
 */
public class SchoolFragment extends Fragment {

    private RecyclerView recyclerView;
    private SchoolViewModel schoolViewModel;

    public SchoolFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Initializing viewmodel using viewmodel factory.
        schoolViewModel = new ViewModelProvider(this, new ViewModelFactory(requireActivity(),
                this)).get(SchoolViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        /* Our observer is put here to handle displaying data when user hits the back button since
         * I am handling the backstack for my fragments as well in the activity lifecycle.
         */
        observeSchool();
    }

    /**
     * This methode observe the viemodel and update this view whenever data change.
     */
    private void observeSchool(){
        schoolViewModel.fetchSchoolData();
        schoolViewModel.getSchools().observe(this, new Observer<List<School>>() {
            @Override
            public void onChanged(List<School> schools) {
                showSchool(schools);
            }
        });
    }

    /**
     * This method set the recycler view and display all school available as well as handle
     * an onClickListener to navigate into the school details page.
     * @param schools
     */
    private void showSchool(List<School> schools) {
        SchoolAdapter schoolAdapter = new SchoolAdapter(getContext(), schools, new SchoolAdapter.RecyclerItemClickListener() {
            @Override
            public void onClickListener(School school, int position) {
                School mySchool = School.getInstance();
                mySchool.setSchool(school);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new SchoolDetailFragment(), "SchoolDetailFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(schoolAdapter);
        schoolAdapter.notifyDataSetChanged();
    }


}
