package com.fall.nycschools.ui.fragments;

import android.view.Gravity;

import androidx.test.core.app.ActivityScenario;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.fall.nycschools.R;
import com.fall.nycschools.ui.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerActions.open;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.contrib.NavigationViewActions.navigateTo;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


/**
 * Tests for the {@link androidx.drawerlayout.widget.DrawerLayout} layout
 * component in {@link MainActivity} which manages the navigation within the app.
 */

@RunWith(AndroidJUnit4ClassRunner.class)
public class NavigationTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule
            = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void test_clickOnNavigationDrawerItem_and_goToHomeFragment() {
        // Open drawer to click on navigation.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left drawer should be closed.
                .perform(open()); // Open the drawer.

        // Start welcome screen
        onView(withId(R.id.nav_view))
                .perform(navigateTo(R.id.nav_home));

        // Check that home fragment was opened.
        onView(withId(R.id.fragment_home))
                .check(matches(isDisplayed()));

    }

    /**
     * To run this test, you will need to deactivate all animations in your phone otherwise it will
     * failed. This is because I am using a progressDialogBar and Espresso does not support it.
     * You can remove the comment in this section and run the test once you are sure that no animation is
     * activated in your emulator.
     */
    /*@Test
    public void test_clickOnNavigationDrawerItem_and_goToSchoolFragment() {
        // Open drawer to click on navigation.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left drawer should be closed.
                .perform(open()); // Open the drawer.

        // Start welcome screen
        onView(withId(R.id.nav_view))
                .perform(navigateTo(R.id.nav_school));

        // Check that home fragment was opened.
        onView(withId(R.id.fragment_school))
                .check(matches(isDisplayed()));

    }*/

    /**
     * To run this test, you will need to deactivate all animations in your phone otherwise it will
     * failed. This is because I am using a progressDialogBar and Espresso does not support it.
     * You can remove the comment in this section and run the test once you are sure that no animation is
     * activated in your emulator.
     */
    /*@Test
    public void test_clickOnNavigationDrawerItem_and_goToSchoolDetailFragment() {
        // Open drawer to click on navigation.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left drawer should be closed.
                .perform(open()); // Open the drawer.

        // Start welcome screen
        onView(withId(R.id.nav_view))
                .perform(navigateTo(R.id.nav_scores));

        // Check that home fragment was opened.
        onView(withId(R.id.fragment_sat))
                .check(matches(isDisplayed()));

    }*/
}
