package com.fall.nycschools.ui.activities;

import androidx.test.core.app.ActivityScenario;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.fall.nycschools.R;

import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4ClassRunner.class)
public class MainActivityTest {

    public void test_isActivityInView(){
        ActivityScenario activityScenario = ActivityScenario.launch(MainActivity.class);
        onView(withId(R.id.drawer_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void test_visibility_toolbar(){
        ActivityScenario activityScenario = ActivityScenario.launch(MainActivity.class);
        //Test is toolbar is visible
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        //test if fragment is visible
        onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));
    }



}